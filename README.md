## Laravel API CRUD
by: Frederyco Miguel
## Instalação

``` Bash
    
- composer install
    
- Copy ".env.example" to ".env"
    
- php artisan migrate

- php artisan db:seed
    
- npm install
    
- npm run watch
```

## Endpoints

<h4>Pegar todos Cadastros</h4>

``` Bash
GET api/crud-test
```

<h4>Pegar unico Cadastro</h4>

``` Bash
GET api/crud-test/{id}
```

<h4>Deletar Cadastro</h4>

``` Bash
delete api/crud-test/{id}
```

<h4>Adicionar Cadastro</h4>

``` Bash
POST api/crud-test
```

<h4>Atualizar Cadastro</h4>

``` Bash
PUT api/crud-test
```

